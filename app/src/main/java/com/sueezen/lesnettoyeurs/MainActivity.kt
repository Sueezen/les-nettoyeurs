package com.sueezen.lesnettoyeurs

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.security.MessageDigest
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import kotlin.math.sign

class MainActivity : AppCompatActivity() {
    var session : String? = null
    var signature : String? = null
    var lm : LocationManager? = null
    var longitude = 0.0
    var latitude = 0.0

    private val INITIAL_PERMS = arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION)
    private val LOCATION_PERMS = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    private val INITIAL_REQUEST = 1337
    private val LOCATION_REQUEST = INITIAL_REQUEST + 3

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val connexion : Button = findViewById(R.id.connection)
        connexion.setOnClickListener{

            val identifiant : EditText = findViewById(R.id.identifiant);
            val password : EditText = findViewById(R.id.password);
            var identifiant_text = identifiant.text.toString();
            var password_text = password.text.toString();

            if (!TextUtils.isEmpty(identifiant_text) && !TextUtils.isEmpty(password_text)) {
                password_text = hashString(password_text);

                try {
                    identifiant_text = URLEncoder.encode(identifiant_text, "UTF-8")
                    password_text = URLEncoder.encode(password_text, "UTF-8")
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }

                val pool_connexion : ExecutorService = Executors.newFixedThreadPool(1);
                val wb_connexion = WebServiceConnexion("http://51.68.124.144/nettoyeurs_srv/connexion.php?login=$identifiant_text&passwd=$password_text")
                val obj_connexion = pool_connexion.submit(wb_connexion)

                var status = obj_connexion.get()["Status"]

                if (status == "OK") {
                    session = obj_connexion.get()["Session"]
                    signature = obj_connexion.get()["Signature"]

                    lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager?

                    if (!canAccessLocation()) {
                        requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                    }

                    if (canAccessLocation()) {
                        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                        val location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            longitude = location.longitude

                        };
                        if (location != null) {
                            latitude = location.latitude

                        };
                    }

                    Log.d("Session : ", session.toString())
                    Log.d("Signature : ", signature.toString())

                    val pool_creation = Executors.newFixedThreadPool(1);
                    val wb_creation = WebServiceCreation("http://51.68.124.144/nettoyeurs_srv/new_nettoyeur.php?session=$session&signature=$signature&lon=$longitude&lat=$latitude")
                    val obj_creation = pool_creation.submit(wb_creation)

                    status = obj_creation.get()["Status"]

                    if (status == "KO - NOT IN 3IA") {
                        val alert = AlertDialog.Builder(this)
                        alert.setTitle("Information - 1ère connexion")
                        alert.setMessage("Vous devez vous rendre au Bât. 3IA, pour pouvoir créer votre premier personnage !")
                        alert.show()
                    } else {
                        val intent : Intent = Intent(this, AccueilActivity::class.java);
                        intent.putExtra("session", session)
                        intent.putExtra("signature", signature)
                        startActivity(intent)
                    }

                    Log.d("Status : ", status.toString())

                    //val alert = AlertDialog.Builder(this)
                    //alert.setTitle("Information Connexion")
                    //alert.setMessage("Signature = $session, signature = $signature")
                    //alert.show()
                } else {
                    val alert = AlertDialog.Builder(this)
                    alert.setTitle("Erreur de connexion")
                    alert.setMessage(status)
                    alert.show()
                    if (status != null) {
                        Log.e("Erreur de connexion", status)
                    }
                }
            } else {
                val alert = AlertDialog.Builder(this)
                alert.setTitle("Erreur de saisie")
                alert.setMessage("Vous devez rentrer un login et mot de passe")
                alert.show()
                Log.e("Erreur de connexion", "Vous n'avez pas rentrer d'identifiant ou mot de passe !")
            }
        }
    }

    private fun hashString(input: String): String {
        return MessageDigest
            .getInstance("SHA-256")
            .digest(input.toByteArray())
            .fold("") { str, it -> str + "%02x".format(it) }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun canAccessLocation(): Boolean {
        return hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasPermission(perm: String): Boolean {
        return PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm)
    }
}