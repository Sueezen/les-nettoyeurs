package com.sueezen.lesnettoyeurs

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.concurrent.Callable
import javax.xml.parsers.DocumentBuilderFactory

class WebServiceLastMessage(private val url: String, private val messageFragment: MessageFragment) : Callable<Map<String, Any>> {

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun call() : Map<String, Any> {
        try {
            val url = URL(url)
            val urlConnection : HttpURLConnection = url.openConnection() as HttpURLConnection
            urlConnection.connect()
            val statusCode = urlConnection.responseCode;
            if(statusCode == 200) {
                val inputStream: InputStream = urlConnection.inputStream
                val dbf = DocumentBuilderFactory.newInstance()
                val db = dbf.newDocumentBuilder()
                val xml = db.parse(inputStream)
                var nList: NodeList = xml.getElementsByTagName("STATUS")
                val nodeStatus: Node = nList.item(0)
                val status = nodeStatus.textContent
                if (status == "OK") {
                    nList = xml.getElementsByTagName("CONTENT")
                    val nodeParams: Node = nList.item(0)
                    val nodes = nodeParams.childNodes
                    val aAjouter = ArrayList<Message>()
                    for (index in 0 until nodes.length) {
                        val nNode = nodes.item(index).childNodes
                        val id = nNode.item(0).textContent.toString()
                        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        val date = format.parse(nNode.item(1).textContent.toString())
                        val author = nNode.item(2).textContent.toString()
                        val msg = nNode.item(3).textContent.toString()
                        aAjouter.add(Message(id.toInt(), author, msg, date))
                        //Log.d("Webservice", "Id = $id, Author = $author, Date = $date, msg = $msg")
                        //messageFragment.addMessage(id.toInt(), date, author, msg)
                        //messages?.ajouteMessage(id.toInt(), date, author, msg)
                        //messageFragment.addMessage(id.toInt(), date, author, msg)
                    }
                    //Log.d("ListMessage", messages?.listMessage.toString())
                    return mapOf("Status" to status, "ListMessage" to aAjouter)
                }
                return mapOf("Status" to status);
            }
        } catch (e: java.lang.Exception) {
            Log.e("WebService", "Erreur de connexion");
            throw Exception("Erreur de connexion");
        }
        return mapOf("Status" to "KO - Erreur de connexion")
    }
}