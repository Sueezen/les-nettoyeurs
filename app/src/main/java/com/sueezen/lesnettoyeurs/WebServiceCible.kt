package com.sueezen.lesnettoyeurs

import android.util.Log
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Callable
import javax.xml.parsers.DocumentBuilderFactory

class WebServiceCible(private val url: String) : Callable<Map<String, String>> {
    override fun call(): Map<String, String> {
        try {
            val url = URL(url)
            val urlConnection : HttpURLConnection = url.openConnection() as HttpURLConnection
            urlConnection.connect()
            val statusCode = urlConnection.responseCode;
            if(statusCode == 200) {
                val inputStream: InputStream = urlConnection.inputStream
                val dbf = DocumentBuilderFactory.newInstance()
                val db = dbf.newDocumentBuilder()
                val xml = db.parse(inputStream)
                var nList: NodeList = xml.getElementsByTagName("STATUS")
                val nodeStatus: Node = nList.item(0)
                val status = nodeStatus.textContent
                if (status == "OK") {
                    nList = xml.getElementsByTagName("PARAMS")
                    val nodeParams: Node = nList.item(0)
                    val nodes = nodeParams.childNodes
                    return mapOf("Status" to status, "Outcome" to nodes.item(0).textContent, "Detected" to nodes.item(1).textContent)
                }
                return mapOf("Status" to status, "Outcome" to "", "Detected" to "");
            }
        } catch (e: java.lang.Exception) {
            Log.e("WebService", "Erreur de nettoyage de cible");
            throw Exception("Erreur de nettoyage de cible");
        }
        return mapOf("Status" to "KO - Erreur de connexion", "Outcome" to "", "Detected" to "")
    }

}