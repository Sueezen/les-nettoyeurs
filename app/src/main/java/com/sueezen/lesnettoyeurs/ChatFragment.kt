package com.sueezen.lesnettoyeurs

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * A simple [Fragment] subclass.
 * Use the [ChatFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChatFragment(session: String?, signature: String?) : Fragment() {
    // TODO: Rename and change types of parameters
    var messageFragment: MessageFragment? = null
    var session = session
    var signature = signature

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //childFragmentManager.beginTransaction().add(R.id.fragmentChat, MessageFragment(), "MessageFragment").commit()
    }

    override fun onResume() {
        super.onResume()
        val parent : AccueilActivity = activity as AccueilActivity
        parent.raffaichirMessages()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_chat, container, false)
        val sendButton : ImageButton = view.findViewById(R.id.sendButton)
        sendButton.setOnClickListener {
            val message : EditText = view.findViewById(R.id.message);
            var message_text = message.text.toString();
            val parent : AccueilActivity = activity as AccueilActivity
            if (!TextUtils.isEmpty(message_text)) {
                parent.sendMessage(message_text)
            } else {
                val alert = AlertDialog.Builder(parent)
                alert.setTitle("Erreur de saisie")
                alert.setMessage("Vous devez rentrer un message !")
                alert.show()
                Log.e("Erreur de connexion", "Vous n'avez pas rentrer de message !")
            }
        }
        return view
    }
}