package com.sueezen.lesnettoyeurs

import java.util.*

class ListeMessages {
    var listMessage = arrayListOf<Message>();

    fun ajouteMessage(id:Int, date: Date, author: String, msg: String) {
        this.listMessage.add(Message(id, author, msg, date))
    }

    operator fun get(i: Int): Message? {
        return if ( this.listMessage[i] in this.listMessage ) {
            this.listMessage[i];
        } else {
            null;
        }
    }

    fun deleteMessageFromIndex(i: Int): Boolean {
        this.listMessage.remove(this.listMessage[i])
        return false
    }

    fun deleteMessageFromId(id: Int): Int {
        for (i in this.listMessage.indices) {
            if (this.listMessage[i].id == id) {
                this.listMessage.remove(this.listMessage[i]);
            }
        }
        return -1
    }

    fun deleteMessages(): Boolean {
        if (this.listMessage.isEmpty()) return false
        this.listMessage.clear()
        return true
    }

    fun size(): Int {
        return this.listMessage.size;
    }

    init {
        //ajouteMessage(1,Calendar.getInstance().time,"Riri", "Bonjour bonjour")
        //ajouteMessage(2,Calendar.getInstance().time,"Fifi", "Broudaf, zog-zog ! ")
        //ajouteMessage(3,Calendar.getInstance().time,"Loulou", "On va faire un contenu un peu plus long, pour voir comment ça passe sur tous les affichages. Hopla ! Et même encore un peu plus long histoire de dire. Après tout, normalement on a tout un écran pour l'afficher, donc on est bien large...")
    }
}