package com.sueezen.lesnettoyeurs

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.Style
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.*
import com.sueezen.lesnettoyeurs.databinding.ActivityAccueilBinding
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.collections.ArrayList


class AccueilActivity : AppCompatActivity() {

    var binding : ActivityAccueilBinding? = null
    var lm : LocationManager? = null
    var session : String? = null
    var signature : String? = null
    var modeButton : Boolean = false

    var longitude = 0.0
    var latitude = 0.0
    var contrats : ArrayList<Contrat> = ArrayList<Contrat>()
    var nettoyeurs : ArrayList<Nettoyeur> = ArrayList<Nettoyeur>()

    private val INITIAL_PERMS = arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION)
    private val LOCATION_PERMS = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    private val INITIAL_REQUEST = 1337
    private val LOCATION_REQUEST = INITIAL_REQUEST + 3

    var locationListener : LocationListener = LocationListener() {
        fun onLocationChanged(location : Location) {
            longitude = location.longitude;
            latitude = location.latitude;
        }
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccueilBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        replaceFragment(HomeFragment(session,signature));
        val intent = intent
        lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager?

        session = intent.getStringExtra("session")
        signature = intent.getStringExtra("signature")

        binding!!.bottomNavigationView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> replaceFragment(HomeFragment(session,signature));
                R.id.profil -> replaceFragment(ProfilFragment(session,signature));
                R.id.chat -> {
                    replaceFragment(ChatFragment(session, signature))
                };
            }
            return@setOnItemSelectedListener true;
        }

        //val messageFragment = supportFragmentManager.findFragmentById(R.id.fragmentMessage) as MessageFragment?

        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }

        if (canAccessLocation()) {
            val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                longitude = location.longitude
            };
            if (location != null) {
                latitude = location.latitude
            };
        }

        val handler = Handler()
        val delay = 15000;

        handler.postDelayed(object : Runnable {
            override fun run() {
                if (canAccessLocation()) {
                    val location = lm?.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        longitude = location.longitude
                        Log.d("Longitude : ", longitude.toString())
                    };
                    if (location != null) {
                        latitude = location.latitude
                        Log.d("Latitude : ", latitude.toString())
                    };
                    maj_position(longitude.toString(),latitude.toString(),signature.toString(),session.toString())
                }
                handler.postDelayed(this, delay.toLong())
            }
        }, delay.toLong())
    }

    fun sendMessage(message : String) {
        val bottomNavigationView : BottomNavigationView = findViewById(R.id.bottomNavigationView)
        val item = bottomNavigationView.menu.findItem(bottomNavigationView.selectedItemId)
        if (item.toString() == "Chat") {
            val pool_new: ExecutorService = Executors.newFixedThreadPool(1);
            val wb_new = WebServiceNewMessage("http://51.68.124.144/nettoyeurs_srv/new_msg.php?session=$session&signature=$signature&message=$message")
            val obj_new = pool_new.submit(wb_new)
            val status = obj_new.get()["Status"].toString()
            if (status == "OK") {
                raffaichirMessages()
            }
        }
    }

    fun modeVoyage() : Map<String, Any> {
        val pool_voyage: ExecutorService = Executors.newFixedThreadPool(1);
        val wb_voyage = WebServiceModeVoyage("http://51.68.124.144/nettoyeurs_srv/mode_voyage.php?session=$session&signature=$signature")
        val obj_voyage = pool_voyage.submit(wb_voyage)
        val status = obj_voyage.get()["Status"].toString()
        if (status != "OK") {
            val alert = AlertDialog.Builder(this)
            alert.setTitle("Erreur : Mode Voyage")
            alert.setMessage(status.toString())
            alert.show()
        } else if(status == "OK") {
            val alert = AlertDialog.Builder(this)
            alert.setTitle("Information")
            alert.setMessage("Vous venez de passer en mode normal, pendant 1 minutes, votre nettoyeur est immobile et vulnérable !")
            alert.show()
        }
        return mapOf("Status" to status)
    }

    fun modeNormal() : Map<String, Any> {
        val pool_voyage: ExecutorService = Executors.newFixedThreadPool(1);
        val wb_voyage = WebServiceModeNormal("http://51.68.124.144/nettoyeurs_srv/remise_en_jeu.php?session=$session&signature=$signature&lon=$longitude&lat=$latitude")
        val obj_voyage = pool_voyage.submit(wb_voyage)
        val status = obj_voyage.get()["Status"].toString()
        if (status != "OK") {
            val alert = AlertDialog.Builder(this)
            alert.setTitle("Erreur : Mode Normal")
            alert.setMessage(status.toString())
            alert.show()
        }
        return mapOf("Status" to status)
    }

    fun afficherStats() : Map<String, Any> {
        val bottomNavigationView : BottomNavigationView = findViewById(R.id.bottomNavigationView)
        val item = bottomNavigationView.menu.findItem(bottomNavigationView.selectedItemId)
        val pool_new: ExecutorService = Executors.newFixedThreadPool(1);
        val wb_new = WebServiceStats("http://51.68.124.144/nettoyeurs_srv/stats_nettoyeur.php?session=$session&signature=$signature")
        val obj_new = pool_new.submit(wb_new)
        val status = obj_new.get()["Status"].toString()
        if (status == "OK") {
            val name = obj_new.get()["Name"].toString()
            val value = obj_new.get()["Value"].toString()
            val long = obj_new.get()["Longitude"].toString()
            val lat = obj_new.get()["Latitude"].toString()
            val status_perso = obj_new.get()["Status Perso"].toString()
            return mapOf("Status" to status, "Name" to name, "Value" to value, "Longitude" to long, "Latitude" to lat, "Status Perso" to status_perso)
        }
        return mapOf("Status" to "KO - Erreur de connexion")
    }

    fun afficherStatsEquipe() : Map<String, Any> {
        val bottomNavigationView : BottomNavigationView = findViewById(R.id.bottomNavigationView)
        val item = bottomNavigationView.menu.findItem(bottomNavigationView.selectedItemId)
        if (item.toString() == "Profil") {
            val pool_new: ExecutorService = Executors.newFixedThreadPool(1);
            val wb_new = WebServiceStatsEquipe("http://51.68.124.144/nettoyeurs_srv/stats_equipe.php?session=$session&signature=$signature")
            val obj_new = pool_new.submit(wb_new)
            val status = obj_new.get()["Status"].toString()
            if (status == "OK") {
                val value = obj_new.get()["Value"].toString()
                val adv = obj_new.get()["Adv"].toString()
                val actif = obj_new.get()["Actif"].toString()
                return mapOf("Status" to status, "Value" to value, "Adv" to adv, "Actif" to actif)
            }
        }
        return mapOf("Status" to "KO - Erreur de connexion")
    }

    private fun replaceFragment(fragment : Fragment) {
        val fragmentManager : FragmentManager = supportFragmentManager;
        val fragmentTransaction : FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout, fragment).setPrimaryNavigationFragment(fragment);
        fragmentTransaction.commit();
    }

    fun raffaichirMessages() {
        val bottomNavigationView : BottomNavigationView = findViewById(R.id.bottomNavigationView)
        val item = bottomNavigationView.menu.findItem(bottomNavigationView.selectedItemId)
        if (item.toString() == "Chat") {
            val messageFragment = supportFragmentManager.findFragmentById(R.id.frame_layout)?.childFragmentManager?.findFragmentById(R.id.fragmentMessage) as MessageFragment
            val pool_last: ExecutorService = Executors.newFixedThreadPool(1);
            val wb_last = WebServiceLastMessage("http://51.68.124.144/nettoyeurs_srv/last_msgs.php?session=$session&signature=$signature", messageFragment)
            val obj_last = pool_last.submit(wb_last)
            val status = obj_last.get()["Status"].toString()
            if (status == "OK") {
                val listMessage = obj_last.get()["ListMessage"] as ArrayList<Message>
                messageFragment.deleteMessages()
                for (m in listMessage) {
                    messageFragment.addMessage(m.id, m.date, m.author, m.msg)
                }
            }
        }
    }

    fun maj_position(lat: String, long: String,signature:String, session: String) {
        var test_lat = "47.8455456347924"
        var test_long = "1.939245600231251"
        val pool_new: ExecutorService = Executors.newFixedThreadPool(1);
        val wb_new = WebServiceMAJPosition("http://51.68.124.144/nettoyeurs_srv/deplace.php?session=$session&signature=$signature&lon=$longitude&lat=$latitude")
        val obj_new = pool_new.submit(wb_new)
        val status = obj_new.get()["Status"].toString()
        if (status == "OK") {
            contrats.clear()
            nettoyeurs.clear()
            contrats = obj_new.get()["Contrats"] as ArrayList<Contrat>
            nettoyeurs = obj_new.get()["Nettoyeurs"] as ArrayList<Nettoyeur>
        }
        for(contrat in contrats){
            createCible(contrat.cible_id.toString(),contrat.long,contrat.lat,contrat)
        }
        for(nettoyeur in nettoyeurs){
            createNettoyeur(nettoyeur.net_id.toString(),nettoyeur.lon,nettoyeur.lat,nettoyeur)
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun canAccessLocation(): Boolean {
        return hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasPermission(perm: String): Boolean {
        return PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm)
    }

    fun createCible(id: String, lon: String, lat: String, cible: Contrat) {
        val annotationApi = mapView?.annotations
        val circleAnnotationManager = annotationApi?.createCircleAnnotationManager(mapView!!)
        val circleAnnotationOptions: CircleAnnotationOptions = CircleAnnotationOptions()
            // Define a geographic coordinate.
            .withPoint(Point.fromLngLat(lon.toDouble(), lat.toDouble()))
            // Style the circle that will be added to the map.
            .withCircleRadius(8.0)
            .withCircleColor("#ffa500")
            .withCircleStrokeWidth(2.0)
            .withCircleStrokeColor("#ffffff")
        circleAnnotationManager?.create(circleAnnotationOptions)
        circleAnnotationManager?.addClickListener(object: OnCircleAnnotationClickListener{
            override fun onAnnotationClick(annotation: CircleAnnotation): Boolean {
                val pool_new: ExecutorService = Executors.newFixedThreadPool(1);
                val wb_new = WebServiceMAJPosition("http://51.68.124.144/nettoyeurs_srv/frappe_cible.php?session=$session&signature=$signature&cible_id=${cible.cible_id}")
                val obj_new = pool_new.submit(wb_new)
                val status = obj_new.get()["Status"].toString()
                if (status == "OK") {
                    circleAnnotationManager.deleteAll()
                }
                return true
            }

        })
    }

    fun createNettoyeur(id: String, lon: String, lat: String, nettoyeur: Nettoyeur) {
        val annotationApi = mapView?.annotations
        val circleAnnotationManager = annotationApi?.createCircleAnnotationManager(mapView!!)
        val circleAnnotationOptions: CircleAnnotationOptions = CircleAnnotationOptions()
            // Define a geographic coordinate.
            .withPoint(Point.fromLngLat(lon.toDouble(), lat.toDouble()))
            // Style the circle that will be added to the map.
            .withCircleRadius(8.0)
            .withCircleColor("#ff0000 ")
            .withCircleStrokeWidth(2.0)
            .withCircleStrokeColor("#ffffff")
        circleAnnotationManager?.create(circleAnnotationOptions)
        circleAnnotationManager?.addClickListener(object: OnCircleAnnotationClickListener{
            override fun onAnnotationClick(annotation: CircleAnnotation): Boolean {
                val pool_new: ExecutorService = Executors.newFixedThreadPool(1);
                val wb_new = WebServiceMAJPosition("http://51.68.124.144/nettoyeurs_srv/frappe_net.php?session=$session&signature=$signature&cible_id=${nettoyeur.net_id}")
                val obj_new = pool_new.submit(wb_new)
                val status = obj_new.get()["Status"].toString()
                if (status == "OK") {
                    circleAnnotationManager.deleteAll()
                }
                return true
            }

        })
    }

}