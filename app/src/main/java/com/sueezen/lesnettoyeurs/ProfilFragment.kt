package com.sueezen.lesnettoyeurs

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * A simple [Fragment] subclass.
 * Use the [ProfilFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfilFragment(session: String?, signature: String?) : Fragment() {
    // TODO: Rename and change types of parameters

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("MissingInflatedId", "SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profil, container, false)

        val parent: AccueilActivity = activity as AccueilActivity
        val map = parent.afficherStats()
        val pseudo = view.findViewById(R.id.pseudo) as TextView
        val kills = view.findViewById(R.id.kills) as TextView
        val longitude = view.findViewById(R.id.longitude) as TextView
        val latitude = view.findViewById(R.id.latitude) as TextView
        val status = view.findViewById(R.id.status) as TextView
        //val points = view.findViewById(R.id.points) as TextView
        //val nb_ennemis = view.findViewById(R.id.nb_ennemis) as TextView
        //val nb_actifs = view.findViewById(R.id.nb_actifs) as TextView
        pseudo.text = map["Name"] as CharSequence?
        kills.text = map["Value"] as CharSequence?
        longitude.text = map["Longitude"] as CharSequence?
        latitude.text = map["Latitude"] as CharSequence?
        if (map["Status Perso"] == "UP") {
            status.text = "Actif"
        } else if (map["Status Perso"] == "PACK") {
            status.text = "Prépare le voyage"
        } else if (map["Status Perso"] == "VOY") {
            status.text = "En voyage"
        } else if (map["Status Perso"] == "NET") {
            status.text = "Essuyage des saletés après un nettoyage"
        } else if (map["Status Perso"] == "DEAD") {
            status.text = "Mort"
        }

        val map2 = parent.afficherStatsEquipe()
        val value = view.findViewById(R.id.killteam) as TextView
        val adv = view.findViewById(R.id.killadv) as TextView
        val actif = view.findViewById(R.id.actifs) as TextView
        value.text = map2["Value"] as CharSequence?
        adv.text = map2["Adv"] as CharSequence?
        actif.text = map2["Actif"] as CharSequence?
        return view
    }
}