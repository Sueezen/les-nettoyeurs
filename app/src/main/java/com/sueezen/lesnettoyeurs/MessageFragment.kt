package com.sueezen.lesnettoyeurs

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

/**
 * A fragment representing a list of Items.
 */
class MessageFragment : Fragment() {

    private var mListener: OnListFragmentInteractionListener? = null
    private var mMessages: ListeMessages? = null
    private var mAdapter: MyMessageRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_message_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            val context = view.getContext()
            view.layoutManager = LinearLayoutManager(context)
            if (mMessages == null) mMessages = ListeMessages()
            if (mAdapter == null) mAdapter = MyMessageRecyclerViewAdapter(mMessages!!, mListener)
            view.adapter = mAdapter
        }
        return view
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addMessage(id: Int, date: Date, title : String, contenu : String) {
        mMessages!!.ajouteMessage(id, date, title, contenu)
        mAdapter!!.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun deleteMessage(idmessageselected : Int) {
        mMessages!!.deleteMessageFromId(id)
        mAdapter!!.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun deleteMessages() {
        mMessages!!.deleteMessages()
        mAdapter!!.notifyDataSetChanged()
    }

    fun getMessages() : ListeMessages? {
        return this.mMessages
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Message?)
    }
}