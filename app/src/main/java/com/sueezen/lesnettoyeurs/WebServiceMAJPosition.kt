package com.sueezen.lesnettoyeurs

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.concurrent.Callable
import javax.xml.parsers.DocumentBuilderFactory

class WebServiceMAJPosition(private val url: String) :
    Callable<Map<String, Any>> {

        @SuppressLint("SimpleDateFormat")
        @RequiresApi(Build.VERSION_CODES.O)
        override fun call() : Map<String, Any> {
            try {
                Log.d("URL",url)
                val url = URL(url)
                val urlConnection : HttpURLConnection = url.openConnection() as HttpURLConnection
                urlConnection.connect()
                val statusCode = urlConnection.responseCode;
                if(statusCode == 200) {
                    val inputStream: InputStream = urlConnection.inputStream
                    val dbf = DocumentBuilderFactory.newInstance()
                    val db = dbf.newDocumentBuilder()
                    val xml = db.parse(inputStream)
                    var nList: NodeList = xml.getElementsByTagName("STATUS")
                    val nodeStatus: Node = nList.item(0)
                    val status = nodeStatus.textContent
                    if (status == "OK") {
                        nList = xml.getElementsByTagName("PARAMS")
                        val nodeParams: Node = nList.item(0)
                        val nodes = nodeParams.childNodes
                        val nNodeContrat = nodes.item(0).childNodes
                        val nNodeNettoyeur = nodes.item(1).childNodes
                        val contrats = ArrayList<Contrat>()
                        val nettoyeurs = ArrayList<Nettoyeur>()
                        for (index in 0 until nNodeContrat.length) {
                            val nNode = nNodeContrat.item(index).childNodes
                            val cible_id = nNode.item(0).textContent.toString()
                            val value = nNode.item(1).textContent.toString()
                            val longitude = nNode.item(2).textContent.toString()
                            val latitude = nNode.item(3).textContent.toString()
                            contrats.add(Contrat(cible_id.toInt(), value.toInt(), longitude, latitude))
                        }
                        for (index in 0 until nNodeNettoyeur.length) {
                            val nNode = nNodeNettoyeur.item(index).childNodes
                            val net_id = nNode.item(0).textContent.toString()
                            val value = nNode.item(1).textContent.toString()
                            val longitude = nNode.item(2).textContent.toString()
                            val latitude = nNode.item(3).textContent.toString()
                            val lifespan = nNode.item(4).textContent.toString()
                            nettoyeurs.add(Nettoyeur(net_id.toInt(), value.toInt(), longitude, latitude, lifespan.toInt()))
                        }
                        return mapOf("Status" to status, "Contrats" to contrats, "Nettoyeurs" to nettoyeurs);
                    }
                    return mapOf("Status" to status);
                }
            } catch (e: java.lang.Exception) {
                Log.e("WebService", "Erreur de connexion");
                throw Exception("Erreur de connexion");
            }
            return mapOf("Status" to "KO - Erreur de connexion")
        }
}