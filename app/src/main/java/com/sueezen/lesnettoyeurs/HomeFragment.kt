package com.sueezen.lesnettoyeurs

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.MapView
import com.mapbox.maps.Style
import com.mapbox.maps.plugin.PuckBearingSource
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorBearingChangedListener
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.maps.plugin.locationcomponent.location2


/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

var mapView: MapView? = null

class HomeFragment(session: String?, signature: String?) : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    private val onIndicatorBearingChangedListener = OnIndicatorBearingChangedListener {
        mapView!!.getMapboxMap().setCamera(CameraOptions.Builder().bearing(it).build())
    }

    private val onIndicatorPositionChangedListener = OnIndicatorPositionChangedListener {
        mapView!!.getMapboxMap().setCamera(CameraOptions.Builder().center(it).build())
        mapView!!.gestures.focalPoint = mapView!!.getMapboxMap().pixelForCoordinate(it)
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val modeButton = view.findViewById(R.id.modeButton) as Button
        val parent: AccueilActivity = activity as AccueilActivity
        if (parent.modeButton) {
            modeButton.isEnabled = false
        }
        var mode = 0;
        val map_stat = parent.afficherStats()
        if (map_stat["Status Perso"] == "VOY") {
            modeButton.text = "Mode Normal"
            mode = 0
        } else {
            modeButton.text = "Mode Voyage"
            mode = 1
        }

        // Mode == 1 (NORMAL)
        // Mode == 0 (VOYAGE)
        modeButton.setOnClickListener {
            if (mode == 1) {
                val map_voyage = parent.modeVoyage()
                if (map_voyage["Status"] == "OK") {
                    parent.modeButton = true
                    modeButton.setEnabled(false)
                    Handler(Looper.getMainLooper()).postDelayed(
                        {
                            parent.modeButton = false
                            modeButton.isEnabled = true
                            modeButton.text = "Mode Normal"
                            mode = 0
                        },
                        60000 // value in milliseconds
                    )
                }
            } else {
                // Mode == 0
                val map_normal = parent.modeNormal()
                if (map_normal["Status"] == "OK") {
                    modeButton.text = "Mode Voyage"
                    mode = 1
                }
            }
        }

        mapView = view.findViewById(R.id.mapView) as MapView
        mapView!!.setOnTouchListener(object : View.OnTouchListener{
            @SuppressLint("ClickableViewAccessibility")
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                return true
            }
        })
        mapView!!.getMapboxMap().loadStyleUri(Style.MAPBOX_STREETS,object : Style.OnStyleLoaded {
            @SuppressLint("ClickableViewAccessibility")
            override fun onStyleLoaded(style: Style) {
                mapView!!.location.updateSettings {
                    enabled = true
                    pulsingEnabled = true
                }
                mapView!!.location2.puckBearingSource = PuckBearingSource.HEADING
                mapView!!.location2.puckBearingSource = PuckBearingSource.COURSE
                mapView!!.location.addOnIndicatorPositionChangedListener(
                    onIndicatorPositionChangedListener
                )
                mapView!!.location.addOnIndicatorBearingChangedListener(
                    onIndicatorBearingChangedListener
                )
                mapView!!.gestures.updateSettings {
                    scrollEnabled = false
                    doubleTapToZoomInEnabled = false
                    quickZoomEnabled = false
                    pinchToZoomEnabled = false
                    pinchToZoomDecelerationEnabled = false
                    simultaneousRotateAndPinchToZoomEnabled = false
                    doubleTouchToZoomOutEnabled = false
                }

            }
        }
        )
        return view
    }
}
